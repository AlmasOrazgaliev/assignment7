package config

import (
	"errors"
	"gitlab.com/golang-libs/databases.git/config"
	"go.uber.org/zap"
	"os"
)

type Pusher struct {
	ServiceURl          string `json:"service_url"`
	SendNotificationURL string `json:"send_notification_url"`
	NotificationInfoURL string `json:"notification_info_url"`
}

type Configuration struct {
	AppPort     string          `json:"app_port" validate:"nonzero"`
	PublishPath string          `json:"public_path" validate:"nonzero"`
	CallbackURL string          `json:"callback_url" validate:"nonzero"`
	DBConfig    config.DBConfig `json:"db" validate:"nonzero"`
	Auth        authConf.Auth   `json:"auth" validate:"nonzero"`
	Pusher      Pusher          `json:"pusher" validate:"nonzero"`
}

func Config() *Configuration {
	return &Configuration{}
}

var ErrEnvVarEmpty = errors.New("getEnv: Environment variable empty")

func getEnv(key string) (string, error) {
	v := os.Getenv(key)
	if v == "" {
		return v, ErrEnvVarEmpty
	}

	return v, nil
}

func GetEnvString(key string) string {
	v, err := getEnv(key)
	if v == "" {
		zap.S().Warn(err.Error())

		return v
	}

	return v
}
