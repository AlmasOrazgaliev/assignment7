package models

import (
	"errors"
)

type Response struct {
	Code    int    `json:"code"`
	Message string `json:"message"`
}

const (
	UnauthorizedError   = "UNAUTHORIZED_ERROR"
	InternalServerError = "INTERNAL_SERVER_ERROR"
	BadRequestError     = "BAD_REQUEST"
)

var (
	ErrInvalidRequestID        = errors.New("invalid request ID")
	ErrInvalidPushIDAndTarget  = errors.New("can't send both push id and target in one request")
	ErrRequestIDAlreadyExists  = errors.New("current request ID already exists")
	ErrInvalidTarget           = errors.New("invalid target")
	ErrNotificationDoesntExist = errors.New("notification doesn't exist")
	ErrInvalidApp              = errors.New("invalid app id")
)
