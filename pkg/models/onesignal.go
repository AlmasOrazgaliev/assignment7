package models

type OneSignalApp struct {
	ID     int64  `db:"id"`
	Name   string `db:"name"`
	AppID  string `db:"app_id"`
	ApiKey string `db:"api_key"`
}
