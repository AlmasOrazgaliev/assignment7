package models

import (
	"time"
)

const (
	includedSegments = "Subscribed Users"
	allTarget        = "All"
	androidTarget    = "Android"
	iosTarget        = "Ios"
	huaweiTarget     = "huawei"
)

type Message struct {
	Status        bool        `json:"status" db:"status"`
	ID            *int        `json:"id" db:"id"`
	ProviderID    *int        `json:"provider_id" db:"provider_id"`
	App           *int        `json:"app" db:"app"`
	PushID        string      `json:"push_id" db:"push_id"`
	ErrorMessage  interface{} `json:"error_message" db:"error"`
	RequestID     string      `json:"request_id" db:"request_id"`
	Message       string      `json:"message" db:"message"`
	ApplicationID int64       `json:"applicationID" db:"application_id"`
	InviteID      int64       `json:"inviteID" db:"invite_id"`
	Target        string      `json:"target" db:"target"`
	MessageID     string      `json:"message_id" db:"message_id"`
	RequestDate   time.Time   `json:"request_date" db:"request_time"`
}

type SendPushNotificationRequest struct {
	App        *int                 `json:"app" validate:"required"`
	ProviderID *int                 `json:"provider_id"`
	RequestID  string               `json:"request_id" validate:"required,validate_request_id"`
	PushID     string               `json:"push_id"`
	Target     string               `json:"target" validate:"check_target"`
	Message    *NotificationMessage `json:"message" validate:"required"`
}

type PushData struct {
	ApplicationID int64 `json:"applicationId,omitempty"`
	InviteID      int64 `json:"inviteId,omitempty"`
}

type NotificationMessage struct {
	Text   string    `json:"text" validate:"required"`
	Header string    `json:"header"`
	URL    string    `json:"url"`
	Data   *PushData `json:"data,omitempty"`
}

type OnesignalPushNotificationRequest struct {
	IsAndroid        *bool                        `json:"is_android,omitempty"`
	IsIOS            *bool                        `json:"is_ios,omitempty"`
	IsHuawei         *bool                        `json:"is_huawei,omitempty"`
	AppId            string                       `json:"app_id"`
	AppURL           string                       `json:"app_url"`
	Contents         OnesignalMultiLanguageObject `json:"contents"`
	Headings         OnesignalMultiLanguageObject `json:"headings"`
	Data             OnesignalPushData            `json:"data"`
	IncludedSegments []string                     `json:"included_segments,omitempty"`
	IncludePlayerIds []string                     `json:"include_player_ids,omitempty"`
}

type OnesignalPushData struct {
	ApplicationId int64 `json:"applicationId,omitempty"`
	InviteId      int64 `json:"inviteId,omitempty"`
}

type OnesignalMultiLanguageObject struct {
	En string `json:"en"`
}

type OnesignalErrorResponse struct {
	Errors []string `json:"errors"`
}

type SendPushNotificationResponse struct {
	ID     string      `json:"id"`
	Errors interface{} `json:"errors"`
}

type OnesignalNotificationsResponse struct {
	AdmBigPicture   string `json:"adm_big_picture"`
	AdmGroup        string `json:"adm_group"`
	AdmGroupMessage struct {
		En string `json:"en"`
	} `json:"adm_group_message"`
	AdmLargeIcon string `json:"adm_large_icon"`
	AdmSmallIcon string `json:"adm_small_icon"`
	AdmSound     string `json:"adm_sound"`
	SpokenText   struct {
	} `json:"spoken_text"`
	AlexaSsml            interface{} `json:"alexa_ssml"`
	AlexaDisplayTitle    interface{} `json:"alexa_display_title"`
	AmazonBackgroundData bool        `json:"amazon_background_data"`
	AndroidAccentColor   string      `json:"android_accent_color"`
	AndroidGroup         string      `json:"android_group"`
	AndroidGroupMessage  struct {
		En string `json:"en"`
	} `json:"android_group_message"`
	AndroidLedColor   interface{} `json:"android_led_color"`
	AndroidSound      interface{} `json:"android_sound"`
	AndroidVisibility int         `json:"android_visibility"`
	AppId             string      `json:"app_id"`
	BigPicture        string      `json:"big_picture"`
	Buttons           interface{} `json:"buttons"`
	Canceled          bool        `json:"canceled"`
	ChromeBigPicture  string      `json:"chrome_big_picture"`
	ChromeIcon        string      `json:"chrome_icon"`
	ChromeWebIcon     string      `json:"chrome_web_icon"`
	ChromeWebImage    string      `json:"chrome_web_image"`
	ChromeWebBadge    string      `json:"chrome_web_badge"`
	ContentAvailable  bool        `json:"content_available"`
	Name              interface{} `json:"name"`
	Contents          struct {
		En string `json:"en"`
	} `json:"contents"`
	Converted int `json:"converted"`
	Data      struct {
		Additional string `json:"additional"`
		Campaign   string `json:"campaign"`
	} `json:"data"`
	DelayedOption     string        `json:"delayed_option"`
	DeliveryTimeOfDay string        `json:"delivery_time_of_day"`
	Errored           int           `json:"errored"`
	ExcludedSegments  []interface{} `json:"excluded_segments"`
	Failed            int           `json:"failed"`
	FirefoxIcon       string        `json:"firefox_icon"`
	GlobalImage       string        `json:"global_image"`
	Headings          struct {
		En string `json:"en"`
	} `json:"headings"`
	Id                     string      `json:"id"`
	IncludePlayerIds       interface{} `json:"include_player_ids"`
	IncludeExternalUserIds interface{} `json:"include_external_user_ids"`
	IncludedSegments       []string    `json:"included_segments"`
	ThreadId               interface{} `json:"thread_id"`
	IosBadgeCount          int         `json:"ios_badgeCount"`
	IosBadgeType           string      `json:"ios_badgeType"`
	IosCategory            string      `json:"ios_category"`
	IosInterruptionLevel   string      `json:"ios_interruption_level"`
	IosRelevanceScore      int         `json:"ios_relevance_score"`
	IosSound               string      `json:"ios_sound"`
	ApnsAlert              struct {
	} `json:"apns_alert"`
	IsAdm                 bool        `json:"isAdm"`
	IsAndroid             bool        `json:"isAndroid"`
	IsChrome              bool        `json:"isChrome"`
	IsChromeWeb           bool        `json:"isChromeWeb"`
	IsAlexa               bool        `json:"isAlexa"`
	IsFirefox             bool        `json:"isFirefox"`
	IsIos                 bool        `json:"isIos"`
	IsSafari              bool        `json:"isSafari"`
	IsWP                  bool        `json:"isWP"`
	IsWPWNS               bool        `json:"isWP_WNS"`
	IsEdge                bool        `json:"isEdge"`
	IsSMS                 bool        `json:"isSMS"`
	LargeIcon             string      `json:"large_icon"`
	Priority              int         `json:"priority"`
	QueuedAt              int         `json:"queued_at"`
	Remaining             int         `json:"remaining"`
	SendAfter             int         `json:"send_after"`
	CompletedAt           int         `json:"completed_at"`
	SmallIcon             string      `json:"small_icon"`
	Successful            int         `json:"successful"`
	Received              int         `json:"received"`
	Tags                  interface{} `json:"tags"`
	Filters               interface{} `json:"filters"`
	TemplateId            interface{} `json:"template_id"`
	Ttl                   int         `json:"ttl"`
	Url                   string      `json:"url"`
	WebUrl                interface{} `json:"web_url"`
	AppUrl                interface{} `json:"app_url"`
	WebButtons            interface{} `json:"web_buttons"`
	WebPushTopic          interface{} `json:"web_push_topic"`
	WpSound               string      `json:"wp_sound"`
	WpWnsSound            string      `json:"wp_wns_sound"`
	PlatformDeliveryStats interface{} `json:"platform_delivery_stats"`
	IosAttachments        struct {
		Id string `json:"id"`
	} `json:"ios_attachments"`
	ThrottleRatePerMinute      interface{} `json:"throttle_rate_per_minute"`
	FcapGroupIds               interface{} `json:"fcap_group_ids"`
	FcapStatus                 string      `json:"fcap_status"`
	SmsFrom                    interface{} `json:"sms_from"`
	SmsMediaUrls               interface{} `json:"sms_media_urls"`
	EmailClickTrackingDisabled interface{} `json:"email_click_tracking_disabled"`
	IsEmail                    bool        `json:"isEmail"`
	EmailSubject               interface{} `json:"email_subject"`
	EmailFromName              interface{} `json:"email_from_name"`
	EmailFromAddress           interface{} `json:"email_from_address"`
}

func CheckTarget(target string) bool {
	targetMap := map[string]struct{}{
		androidTarget: {},
		iosTarget:     {},
		huaweiTarget:  {},
		allTarget:     {},
	}

	_, ok := targetMap[target]

	return ok
}

func MapMessage(status bool, error interface{}, messageID string, notification *SendPushNotificationRequest) *Message {
	message := &Message{
		ProviderID:   notification.ProviderID,
		App:          notification.App,
		Status:       status,
		ErrorMessage: error,
		RequestID:    notification.RequestID,
		Message:      notification.Message.Text,
		MessageID:    messageID,
		RequestDate:  time.Now(),
	}

	if notification.Message.Data != nil {
		message.ApplicationID = notification.Message.Data.ApplicationID
		message.InviteID = notification.Message.Data.InviteID
	}

	if notification.PushID != "" {
		message.PushID = notification.PushID
	}

	if notification.Target != "" {
		message.Target = notification.Target
	}

	return message
}

func MapToOnesignalNotificationSend(appID string, notification *SendPushNotificationRequest) *OnesignalPushNotificationRequest {
	notificationRequest := &OnesignalPushNotificationRequest{
		AppId:    appID,
		AppURL:   notification.Message.URL,
		Contents: OnesignalMultiLanguageObject{En: notification.Message.Text},
		Headings: OnesignalMultiLanguageObject{En: notification.Message.Header},
	}

	if notification.Message.Data != nil {
		notificationRequest.Data = OnesignalPushData{
			ApplicationId: notification.Message.Data.ApplicationID,
			InviteId:      notification.Message.Data.InviteID,
		}
	}

	switch notification.Target {
	case androidTarget:
		isAndroid := true

		notificationRequest.IsAndroid = &isAndroid
	case iosTarget:
		isIOS := true

		notificationRequest.IsIOS = &isIOS
	case huaweiTarget:
		isHuawei := true

		notificationRequest.IsHuawei = &isHuawei
	}

	if notification.Target != "" {
		notificationRequest.IncludedSegments = []string{includedSegments}
	}

	if notification.PushID != "" {
		notificationRequest.IncludePlayerIds = []string{notification.PushID}
	}

	return notificationRequest
}
