package validator

import (
	"github.com/go-playground/validator/v10"
	"regexp"
)

func validateRequestID(fl validator.FieldLevel) bool {
	value := fl.Field().String()

	r := regexp.MustCompile("^[a-fA-F0-9]{8}-[a-fA-F0-9]{4}-4[a-fA-F0-9]{3}-[8|9aAbB][a-fA-F0-9]{3}-[a-fA-F0-9]{12}$")

	return r.MatchString(value)
}
