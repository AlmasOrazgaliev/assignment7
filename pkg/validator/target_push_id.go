package validator

import (
	"github.com/go-playground/validator/v10"
	"pusher/pkg/models"
)

func checkTarget(fl validator.FieldLevel) bool {
	target := fl.Field().String()
	if target == "" {
		return true
	}

	return models.CheckTarget(target)

}
