package validator

import (
	"errors"
	"github.com/go-playground/validator/v10"
	"go.uber.org/zap"
	"pusher/pkg/models"
)

type Validator struct {
	Validator *validator.Validate
}

func NewValidator() *Validator {
	v := validator.New()

	err := v.RegisterValidation("check_target", checkTarget, false)
	if err != nil {
		zap.S().Info(err)
	}

	err = v.RegisterValidation("validate_request_id", validateRequestID, false)
	if err != nil {
		zap.S().Info(err)
	}

	return &Validator{
		Validator: v,
	}
}

func ValidatorErr(fe validator.FieldError) error {
	switch fe.Tag() {
	case "check_target ":
		return models.ErrInvalidTarget
	case "check_app":
		return models.ErrInvalidApp
	case "validate_request_id":
		return models.ErrInvalidRequestID
	case "required":
		return errors.New(models.BadRequestError)
	}

	return fe
}
