package v1

import (
	"bytes"
	"context"
	"encoding/json"
	"errors"
	"fmt"
	"go.uber.org/zap"
	"io"
	"net/http"
	"pusher/config"
	"pusher/internal/database"
	"pusher/pkg/models"
	"strings"
)

type Pusher interface {
	Send(ctx context.Context, notification *models.SendPushNotificationRequest) error
	Status(ctx context.Context, requestID string) (*models.OnesignalNotificationsResponse, error)
}

type pushManager struct {
	config           *config.Configuration
	reportRepository database.Pusher
	appRepository    database.OneSignalApp
}

func NewPushManager(config *config.Configuration, reportRepository database.Pusher, appRepository database.OneSignalApp) Pusher {
	return &pushManager{
		config:           config,
		reportRepository: reportRepository,
		appRepository:    appRepository,
	}
}

func (p *pushManager) Send(ctx context.Context, notification *models.SendPushNotificationRequest) error {
	count, err := p.reportRepository.NotificationCount(notification.RequestID)
	if err != nil {
		zap.S().Errorf("[ERROR] error while checking notification count %v", err)

		return err
	}

	if count > 0 {
		return models.ErrRequestIDAlreadyExists
	}

	message, err := p.sendNotification(ctx, notification)
	if err != nil {
		zap.S().Errorf("[ERROR] error while making Onesignal request- %v", err)

		return err
	}

	err = p.reportRepository.SaveNotification(message)
	if err != nil {
		zap.S().Errorf("[ERROR] could not save record in the datebase  - %v", err)

		return err
	}

	if message.ErrorMessage != nil {
		return fmt.Errorf("%v", message.ErrorMessage)
	}

	return nil
}

func (p *pushManager) Status(ctx context.Context, requestID string) (*models.OnesignalNotificationsResponse, error) {
	messageID, app, err := p.reportRepository.MessageInfoByRequestID(requestID)
	if err != nil {
		zap.S().Errorf("[ERROR] could not get notification info with request id %s - %v", requestID, err)

		return nil, err
	}

	onesignalApp, err := p.appRepository.Get(int64(app))
	if err != nil {
		zap.S().Errorf("[ERROR] could not get onesignal app info with app id %d - %v", app, err)

		return nil, err
	}

	notification, err := p.notificationInfo(ctx, onesignalApp.AppID, messageID, onesignalApp.ApiKey)
	if err != nil {
		return nil, err
	}

	err = p.reportRepository.UpdateOnesignalResponse(requestID, notification)
	if err != nil {
		return nil, err
	}

	return notification, nil
}

func (p *pushManager) notificationInfo(ctx context.Context, appID, notificationID, apiKey string) (*models.OnesignalNotificationsResponse, error) {
	request, err := http.NewRequestWithContext(ctx, http.MethodGet, fmt.Sprintf("%s%s%s", p.config.Pusher.ServiceURl, p.config.Pusher.NotificationInfoURL, notificationID), nil)
	if err != nil {
		return nil, err
	}

	request.Header.Add("Content-Type", "application/json")

	request.Header.Add("Authorization", fmt.Sprintf("Basic %s", apiKey))

	q := request.URL.Query()

	q.Set("app_id", appID)

	request.URL.RawQuery = q.Encode()

	responseBody, err := makeRequest(request)
	if err != nil {
		zap.S().Errorf("[ERROR] error while making HTTP request to Onesignal - %v", err)

		return nil, err
	}

	notification := new(models.OnesignalNotificationsResponse)

	err = json.Unmarshal(responseBody, notification)
	if err != nil {
		zap.S().Errorf("[ERROR] error while unmarshalling JSON  - %v", err)

		return nil, err
	}

	return notification, nil
}

func (p *pushManager) sendNotification(ctx context.Context, notification *models.SendPushNotificationRequest) (*models.Message, error) {
	if notification.App == nil {
		zap.S().Errorf("[ERROR] error while getting app info from database - app id is required, requestID: %s", notification.RequestID)

		return nil, errors.New("app id is required")
	}

	onesignalApp, err := p.appRepository.Get(int64(*notification.App))
	if err != nil {
		zap.S().Errorf("[ERROR] error while getting app info from database  - %v", err)

		return nil, err
	}

	body, err := json.Marshal(models.MapToOnesignalNotificationSend(onesignalApp.AppID, notification))
	if err != nil {
		zap.S().Errorf("[ERROR] error while marshalling JSON  - %v", err)

		return nil, err
	}

	request, err := http.NewRequestWithContext(ctx, http.MethodPost, fmt.Sprintf("%s%s", p.config.Pusher.ServiceURl, p.config.Pusher.SendNotificationURL), bytes.NewBuffer(body))
	if err != nil {
		zap.S().Errorf("[ERROR] error while creating HTTP request - %v", err)

		return nil, err
	}

	request.Header.Add("Content-Type", "application/json")
	request.Header.Add("Authorization", fmt.Sprintf("Basic %s", onesignalApp.ApiKey))

	responseBody, err := makeRequest(request)
	if err != nil {
		zap.S().Errorf("[ERROR] error while making HTTP request to Onesignal - %v", err)

		return models.MapMessage(false, err, "", notification), nil
	}

	response := new(models.SendPushNotificationResponse)

	err = json.Unmarshal(responseBody, response)
	if err != nil {
		zap.S().Errorf("[ERROR] error while unmarshalling JSON  - %v", err)

		return nil, err
	}

	return models.MapMessage(true, response.Errors, response.ID, notification), nil
}

func makeRequest(request *http.Request) ([]byte, error) {
	client := &http.Client{}

	res, err := client.Do(request)
	if err != nil {
		zap.S().Errorf("[ERROR] error could not make HTTP request  - %v", err)

		return nil, err
	}

	body, err := io.ReadAll(res.Body)
	if err != nil {
		zap.S().Error(err.Error())

		return nil, err
	}

	if res.StatusCode != 200 {
		errorResponse := new(models.OnesignalErrorResponse)

		err = json.Unmarshal(body, errorResponse)
		if err != nil {
			zap.S().Error(err.Error())

			return nil, err
		}

		return nil, errors.New(strings.Join(errorResponse.Errors, "\n"))
	}

	return body, nil
}
