package middleware

import (
	"bytes"
	"encoding/json"
	"github.com/gin-gonic/gin"
	"go.uber.org/zap"
	"io"
	"net/http"
	"pusher/config"
	"pusher/pkg/models"
)

const (
	area      = "pusher"
	topic     = "message"
	operation = "send"
)

type AuthController struct {
	config *config.Configuration
}

type AuthorizeController interface {
	CheckReceiver(ctx *gin.Context)
}

func NewAuthController(config *config.Configuration) AuthorizeController {
	return &AuthController{
		config: config,
	}
}

type checkClientResponse struct {
	OwnerId int `json:"owner_id"`
}

func (au *AuthController) CheckReceiver(ctx *gin.Context) {
	body, err := json.Marshal(authModels.Scope{Area: area, Topic: topic, Operation: operation})
	if err != nil {
		ctx.AbortWithStatusJSON(http.StatusInternalServerError, models.Response{Code: http.StatusInternalServerError, Message: models.InternalServerError})
		zap.S().Error(err)

		return
	}

	req, err := http.NewRequest(http.MethodPost, au.config.Auth.GetClientCheckFullURL(), bytes.NewBuffer(body))
	if err != nil {
		ctx.AbortWithStatusJSON(http.StatusInternalServerError, models.Response{Code: http.StatusInternalServerError, Message: models.InternalServerError})
		zap.S().Error(err)

		return
	}

	req.Header.Set("Client-Authorization", ctx.GetHeader("Client-Authorization"))

	client := &http.Client{}

	response, err := client.Do(req)
	if err != nil || response.StatusCode != http.StatusOK {
		ctx.AbortWithStatusJSON(http.StatusUnauthorized, models.Response{Code: http.StatusUnauthorized, Message: models.UnauthorizedError})
		zap.S().Error(models.UnauthorizedError)

		return
	}

	checkClientResponse := make([]checkClientResponse, 0)

	responseBody, err := io.ReadAll(response.Body)
	if err != nil {
		ctx.AbortWithStatusJSON(http.StatusInternalServerError, models.Response{Code: http.StatusInternalServerError, Message: models.InternalServerError})
		zap.S().Error(err)

		return
	}

	err = json.Unmarshal(responseBody, &checkClientResponse)
	if err != nil {
		ctx.AbortWithStatusJSON(http.StatusInternalServerError, models.Response{Code: http.StatusInternalServerError, Message: models.InternalServerError})
		zap.S().Error(err)

		return
	}

	if len(checkClientResponse) > 0 {
		ctx.Set("provider_id", checkClientResponse[0].OwnerId)
	}

}
