package http

import (
	"fmt"
	healthCheck "github.com/RaMin0/gin-health-check"
	"github.com/gin-contrib/cors"
	ginzap "github.com/gin-contrib/zap"
	"github.com/gin-gonic/gin"
	"go.uber.org/zap"
	"log"
	"net/http"
	"pusher/config"
	"pusher/internal/database"
	"pusher/internal/http/middleware"
	pusherResource "pusher/internal/http/v1"
	managers "pusher/internal/managers/v1"
	"time"
)

const (
	maxAge       = 300
	readTimeout  = 5 * time.Second
	writeTimeout = 30 * time.Second
)

type Server struct {
	config *config.Configuration
	router *gin.Engine
	db     database.DataStore
}

func NewServer(config *config.Configuration, db database.DataStore) *Server {
	return &Server{
		config: config,
		router: gin.New(),
		db:     db,
	}
}

func (srv *Server) setupRouter() {
	logger, err := zap.NewProduction()
	if err != nil {
		log.Println(err.Error())
	}

	srv.router.Use(cors.New(cors.Config{
		AllowOrigins:     []string{"*"},
		AllowMethods:     []string{"GET", "POST", "PUT", "PATCH", "DELETE", "OPTIONS"},
		AllowHeaders:     []string{"Accept", "Authorization", "Content-Type", "X-CSRF-Token", "Origin"},
		AllowCredentials: true,
		MaxAge:           maxAge,
	}))

	srv.router.Use(ginzap.Ginzap(logger, time.RFC3339, true), gin.Recovery())
	srv.router.Use(healthCheck.Default())
	srv.config.PublishPath = config.GetEnvString("PUBLISH_PATH")

	controller := middleware.NewAuthController(srv.config)

	v1 := srv.router.Group(srv.config.PublishPath + "/v1")

	reportManager := managers.NewPushManager(srv.config, srv.db.PusherRepository(), srv.db.OneSignalAppRepository())

	pusherResource.NewPushResource(srv.config, reportManager).Init(v1, controller)
}

func (srv *Server) Run() {
	srv.setupRouter()

	server := &http.Server{
		Addr:         fmt.Sprintf(":%s", srv.config.AppPort),
		Handler:      srv.router,
		ReadTimeout:  readTimeout,
		WriteTimeout: writeTimeout,
	}

	zap.S().Infof("Listening and serving HTTP on %s", srv.config.AppPort)

	err := server.ListenAndServe()
	if err != nil && err != http.ErrServerClosed {
		zap.S().Fatalf("Couldn't run HTTP server %v", err)
	}

}
