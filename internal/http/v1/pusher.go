package v1

import (
	"errors"
	"github.com/gin-gonic/gin"
	"github.com/go-playground/validator/v10"
	"go.uber.org/zap"
	"net/http"
	"pusher/config"
	"pusher/internal/http/middleware"
	managers "pusher/internal/managers/v1"
	"pusher/pkg/models"
	validation "pusher/pkg/validator"
)

type Push struct {
	config      *config.Configuration
	pushManager managers.Pusher
	validate    *validation.Validator
}

func NewPushResource(config *config.Configuration, pushManager managers.Pusher) *Push {
	return &Push{
		config:      config,
		pushManager: pushManager,
		validate:    validation.NewValidator(),
	}
}

func (p *Push) Init(router *gin.RouterGroup, authorize middleware.AuthorizeController) {
	route := router.Use(authorize.CheckReceiver)

	route.POST("/send", p.Send)
	route.GET("/status", p.Status)
}

func (p *Push) Send(ctx *gin.Context) {
	notification := new(models.SendPushNotificationRequest)

	auth := ctx.GetHeader("Client-Authorization")
	zap.S().Infof("Auth %s", auth)

	notification.RequestID = ctx.GetHeader("RequestID")

	err := ctx.BindJSON(notification)
	if err != nil {
		zap.S().Error(err)
		ctx.JSON(http.StatusBadRequest, models.Response{Code: http.StatusBadRequest, Message: models.BadRequestError})
		return
	}

	zap.S().Infof("Notification Send Message body: %v", notification)

	if notification.Target == "" && notification.PushID == "" {
		zap.S().Error(err)
		ctx.JSON(http.StatusBadRequest, models.Response{Code: http.StatusBadRequest, Message: models.BadRequestError})
		return
	}

	if notification.Target != "" && notification.PushID != "" {
		zap.S().Error(err)
		ctx.JSON(http.StatusBadRequest, models.Response{Code: http.StatusBadRequest, Message: models.ErrInvalidPushIDAndTarget.Error()})
		return
	}

	err = p.validate.Validator.Struct(notification)
	if err != nil {
		var e validator.ValidationErrors

		if errors.As(err, &e) {
			for _, fe := range e {
				ctx.JSON(http.StatusBadRequest, models.Response{Code: http.StatusBadRequest, Message: validation.ValidatorErr(fe).Error()})

				return
			}
		}
	}

	notification.ProviderID = retrieveProviderID(ctx.Get("provider_id"))

	err = p.pushManager.Send(ctx, notification)
	if err != nil {
		errSwitch(ctx, err)

		return
	}

	ctx.JSON(http.StatusOK, models.Response{Code: http.StatusOK, Message: http.StatusText(http.StatusOK)})
}

func (p *Push) Status(ctx *gin.Context) {
	requestID := ctx.GetHeader("RequestID")

	response, err := p.pushManager.Status(ctx, requestID)
	if err != nil {
		errSwitch(ctx, err)

		return
	}

	ctx.JSON(http.StatusOK, response)
}

func retrieveProviderID(providerID interface{}, exists bool) *int {
	if !exists {
		return nil
	}

	id, ok := providerID.(int)
	if !ok {
		return nil
	}

	return &id
}

func errSwitch(ctx *gin.Context, err error) {
	switch err {
	case models.ErrRequestIDAlreadyExists:
		ctx.JSON(http.StatusConflict, models.Response{Code: http.StatusConflict, Message: models.ErrRequestIDAlreadyExists.Error()})
	default:
		ctx.JSON(http.StatusBadRequest, models.Response{Code: http.StatusBadRequest, Message: err.Error()})
	}

	zap.S().Error(err)
}
