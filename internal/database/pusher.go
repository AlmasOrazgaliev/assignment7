package database

import (
	"database/sql"
	"encoding/json"
	"errors"
	"fmt"
	"github.com/jackc/pgx"
	"github.com/jmoiron/sqlx"
	"go.uber.org/zap"
	"pusher/pkg/models"
	"time"
)

type Pusher interface {
	NotificationCount(requestID string) (int, error)
	SaveNotification(message *models.Message) error
	MessageInfoByRequestID(requestID string) (string, int, error)
	UpdateOnesignalResponse(requestID string, notification *models.OnesignalNotificationsResponse) error
}

type pusher struct {
	db *sqlx.DB
}

func NewPusher(db *sqlx.DB) Pusher {
	return &pusher{
		db: db,
	}
}

func (p *pusher) NotificationCount(requestID string) (int, error) {
	var count int

	err := p.db.QueryRowx(`SELECT COUNT(*) FROM pusher_db.pusher.messages WHERE request_id = $1`, requestID).Scan(&count)
	if err != nil {
		zap.S().Errorf("[ERROR] error getting notification whith request id %s - %v", requestID, err)

		return 0, err
	}

	return count, nil
}

func (p *pusher) UpdateOnesignalResponse(requestID string, notification *models.OnesignalNotificationsResponse) error {
	query := `UPDATE pusher_db.pusher.messages SET onesignal_notification = :onesignal_notification WHERE request_id = :request_id`

	marshaledValue, err := json.Marshal(notification)
	if err != nil {
		zap.S().Error(err)

		return err
	}

	args := map[string]interface{}{
		"onesignal_notification": string(marshaledValue),
		"request_id":             requestID,
	}

	_, err = p.db.NamedExec(query, args)
	if err != nil {
		zap.S().Errorf("[ERROR] error inserting notification %v", err)

		return err
	}

	return nil
}

func (p *pusher) MessageInfoByRequestID(requestID string) (string, int, error) {
	var messageID string

	var app int

	err := p.db.QueryRowx(`SELECT message_id, app FROM pusher_db.pusher.messages WHERE request_id = $1`, requestID).Scan(&messageID, &app)
	switch err {
	case nil:
	case sql.ErrNoRows:
		return "", 0, models.ErrNotificationDoesntExist
	default:
		return "", 0, err
	}

	return messageID, app, nil
}

func (p *pusher) SaveNotification(message *models.Message) error {
	query := `INSERT INTO pusher_db.pusher.messages (request_id, provider_id, message, app, target, push_id, status, error, message_id, request_time, application_id, invite_id) 
		   	  VALUES (:request_id, :provider_id, :message, :app, :target, :push_id, :status, :error, :message_id, :request_time, :application_id, :invite_id)`

	args := map[string]interface{}{
		"request_id":     message.RequestID,
		"provider_id":    message.ProviderID,
		"message":        message.Message,
		"app":            message.App,
		"target":         message.Target,
		"push_id":        message.PushID,
		"status":         message.Status,
		"error":          fmt.Sprintf("%v", message.ErrorMessage),
		"message_id":     message.MessageID,
		"request_time":   time.Now(),
		"application_id": message.ApplicationID,
		"invite_id":      message.InviteID,
	}

	_, err := p.db.NamedExec(query, args)
	if err != nil {
		zap.S().Errorf("[ERROR] error inserting notification %v", err)

		return errSwitch(err)
	}

	return nil
}

func errSwitch(err error) error {
	e := err.(pgx.PgError)
	if e.Code == "23505" {
		return models.ErrRequestIDAlreadyExists
	}

	return errors.New(e.Error())
}
