package database

import (
	_ "github.com/jackc/pgx/stdlib"
	"github.com/jmoiron/sqlx"
	"go.uber.org/zap"
)

type DataStore interface {
	Base
	PusherRepository() Pusher
	OneSignalAppRepository() OneSignalApp
}

type Base interface {
	Connect() error
	Close() error
}

type DB struct {
	driver, dbStr string
	db            *sqlx.DB
	pusher        Pusher
	onesignalApp  OneSignalApp
}

func NewDb(driver, dbStr string) DataStore {
	return &DB{
		driver: driver,
		dbStr:  dbStr,
	}
}

func (d *DB) PusherRepository() Pusher {
	if d.pusher == nil {
		d.pusher = NewPusher(d.db)
	}

	return d.pusher
}

func (d *DB) OneSignalAppRepository() OneSignalApp {
	if d.onesignalApp == nil {
		d.onesignalApp = NewOneSignalApp(d.db)
	}

	return d.onesignalApp
}

func (d *DB) Connect() error {
	db, err := sqlx.Connect(d.driver, d.dbStr)
	if err != nil {
		zap.S().Errorf("failed to create database connection %s", err)

		return err
	}

	err = db.Ping()
	if err != nil {
		zap.S().Errorf("could not ping database connection %s", err)

		return err
	}

	d.db = db

	zap.S().Info("Established Database connection")

	return nil
}

func (d *DB) Close() error {
	return d.db.Close()
}
