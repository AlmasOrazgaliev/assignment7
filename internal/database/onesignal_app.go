package database

import (
	"github.com/jmoiron/sqlx"
	"pusher/pkg/models"
)

type OneSignalApp interface {
	Get(id int64) (models.OneSignalApp, error)
}

type oneSignalApp struct {
	db *sqlx.DB
}

func NewOneSignalApp(db *sqlx.DB) OneSignalApp {
	return &oneSignalApp{
		db: db,
	}
}

func (o *oneSignalApp) Get(id int64) (models.OneSignalApp, error) {
	var app models.OneSignalApp

	query := `SELECT id, name, app_id, api_key FROM pusher_db.pusher.onesignal_apps WHERE id = $1`

	err := o.db.Get(&app, query, id)
	if err != nil {
		return models.OneSignalApp{}, err
	}

	return app, nil
}
