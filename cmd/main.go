package main

import (
	"fmt"
	"gitlab.com/golang-libs/mosk.git"
	"go.uber.org/zap"
	"go.uber.org/zap/zapcore"
	"log"
	"os"
	"pusher/config"
	"pusher/internal/database"
	"pusher/internal/http"
)

func encoderConf() zapcore.EncoderConfig {
	return zapcore.EncoderConfig{
		TimeKey:        "timestamp",
		LevelKey:       "severity",
		NameKey:        "logger",
		CallerKey:      "caller",
		MessageKey:     "message",
		StacktraceKey:  "stacktrace",
		LineEnding:     zapcore.DefaultLineEnding,
		EncodeLevel:    zapcore.LowercaseLevelEncoder,
		EncodeTime:     zapcore.ISO8601TimeEncoder,
		EncodeDuration: zapcore.SecondsDurationEncoder,
		EncodeCaller:   zapcore.ShortCallerEncoder,
	}
}

func InitConfiguredLogger(externalConfig zap.Config) error {
	logger, err := externalConfig.Build()
	if err != nil {
		return err
	}

	zap.ReplaceGlobals(logger)
	zap.RedirectStdLog(logger)

	return nil
}

func initDefaultLogger(debug bool) error {
	level := zap.InfoLevel
	if debug {
		level = zap.DebugLevel
	}

	const samplingCongValue = 100

	var defaultJsonConfig = zap.Config{
		Level:         zap.NewAtomicLevelAt(level),
		Development:   debug,
		DisableCaller: true,
		Sampling: &zap.SamplingConfig{
			Initial:    samplingCongValue,
			Thereafter: samplingCongValue,
		},
		Encoding:         "json",
		EncoderConfig:    encoderConf(),
		OutputPaths:      []string{"stderr"},
		ErrorOutputPaths: []string{"stderr"},
	}

	zap.AddStacktrace(zap.ErrorLevel)

	return InitConfiguredLogger(defaultJsonConfig)
}

func main() {
	configuration := config.Config()

	mosk.LoadLocalConfig(configuration)

	err := initDefaultLogger(os.Getenv("DEBUG") != "")
	if err != nil {
		log.Fatalf("Can't initialize logger: %v", err)
	}

	db := database.NewDb(configuration.DBConfig.DriverName, fmt.Sprintf("postgres://%s:%s@%s:%s/%s?sslmode=disable",
		configuration.DBConfig.User, configuration.DBConfig.Pass, configuration.DBConfig.Host, configuration.DBConfig.Port, configuration.DBConfig.Name))

	err = db.Connect()
	if err != nil {
		zap.S().Fatalf("Can't initialize logger: %v", err)
	}

	defer func() {
		err = db.Close()
		if err != nil {
			zap.S().Fatalf("Couldn't close database connection %v", err)
		}
	}()

	srv := http.NewServer(configuration, db)

	srv.Run()

}
